module bitbucket.org/JusticeMax/bifrost

go 1.14

require (
	github.com/apache/pulsar-client-go v0.2.0
	github.com/google/uuid v1.1.2 // indirect
	github.com/pkg/errors v0.9.1
)
